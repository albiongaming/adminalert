package uk.co.dracoril21.bukkit.adminalert;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


import org.bukkit.event.Listener;

/**
 * @author Ethan O'Donnell
 */
public class AdminAlert extends JavaPlugin {
        /*
         * Empty Function tells netbeans this is the main class
         */
        public static void main(String[] args) {}
    
	FileConfiguration config;

	@Override
	public void onEnable() {

		/*
                 * Load Commands Here
                 */

		/* 
		 * Loading the Config
		 * If Config is not there, creating one.
		 * Config controls Variables
                 * @todo Make config persistent
		 */

		try {
                        File f = new File("plugins" + File.separator + "AdminAlert" + File.separator + "config.yml");
                        config = getConfig();
                        if(f.exists()){
                            config.set("version", 0.1);
                            saveConfig();
                        }else{
                            this.saveDefaultConfig();
                        }
			

			/*
			 * Variable for PluginManager... Manager Events now use manager variable
			 */

			final PluginManager manager = this.getServer().getPluginManager();

			/*
			 * Call External Event Classes
			 */

		}

		/*
		 * Printing Errors in Config for config
		 */

		catch (final Exception el) {

			el.printStackTrace();
		}
	}

	@Override
	public void onDisable() {

		/* 
		 * Create a YAML Config Save for Disabling
		 */

	}
}
